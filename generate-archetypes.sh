#!/bin/bash

# gestion des parametres 

b_gen_arc=0
b_exe_mvn=0
b_clean=0
t_projets=""

while getopts ":gecd" FLAG
do 
		case $FLAG in 
				g) b_gen_arc=1 ; echo "Generate Archetype" ;; 
				e) b_exe_mvn=1 ; echo "Execute Maven Cmd" ;; 
				c) b_clean=1 ; echo "clean directory" ;; 
				d) set -x ; echo "mode debug" ;;
				\?) echo "erreur" ; exit 2 ;; 
		esac 
done 

shift $((OPTIND-1))
if [[ $# -gt 1 ]]
then 
	echo "commande en trop"
	exit 3
fi 

# bloc fonction 

# generation des archetypes 
_gen_mvn_arch(){
	i=0
	
	while read -r line 
	do
		[[ -d "project/p${i}" ]] && rm -rf "project/p${i}"
			
		# on filtre la ligne en eliminant les lignes vide ou de types commentaires avec un #
		commande=$( echo "$line" | grep -E -v '^(#|$|[ *$])' )
		
		# si la chaine est superieure a 0, on renseigne un nouvel objet de la collection 

		if [[ -n "$commande" ]]
		then
			
			groupe=$(echo "$commande" | cut -f1 -d":")
			artifact=$(echo "$commande" | cut -f2 -d":")
	
			# ----- objet -----------
			mvn -s ./settings.xml -DoutputDirectory=./project/ -B archetype:generate \
				-DgroupId="test" \
				-DartifactId="p${i}" \
				-Dversion="1.0" \
				-Dpackage="pkg" \
				-DarchetypeGroupId="${groupe}" \
				-DarchetypeArtifactId="${artifact}"
				
			i=$((i+1))
		fi 
	done < ./lst-archetypes.txt

}

# execution des commandes maven
_exe_mvn_cmd()
{
		t_projets=$(ls "./project/") 
	
		for projet in $t_projets
		do
			
			echo "projet : ${projet}"
			# on lance les commandes maven sur les projets 
			while read -r line  
			do  
				# on filtre la ligne en eliminant les lignes vide ou de types commentaires avec un #
				commande=$(echo "$line" | grep -E -v '^(#|$|[ *$])')
				
				# si la chaine est superieure a 0, on renseigne un nouvel objet de la collection
				if [[ -n "$commande" ]]
				then
					
					# on lance la commande sur un projet 
					mvn "$commande" -f "./project/${projet}/pom.xml" -s "./settings.xml"
			
				fi 
			done < ./lst-cmd-mvn.txt
			
			# rm -rf "$M2_HOME"/repository/mytest/
	
		done 
}

_clean()
{
	rm -rf "./project/p"*
}

echo "lancement du programme"

if [[ $b_gen_arc -eq 1 ]] 
then 
	echo "generation archetype..."
	_gen_mvn_arch
	echo "generation archetype...OK"
fi 

if [[ $b_exe_mvn -eq 1 ]] 
then 
	echo "execution cmd maven..."
	_exe_mvn_cmd
	echo "execution cmd maven...OK"
fi 


if [[ $b_clean -eq 1 ]] 
then 
	echo "clean..."
	_clean
	echo "clean...OK"
fi 

