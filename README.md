**[refonte du projet en cours]**

**pitch** : pouvoir generer un repository maven complet pour pouvoir bosser en offline ou dans un réseau fermé 

techno utilisées : docker, maven, pipeline cicd 

### pour les templates 
- pour trouver un template de projet en filtrant sur org.apache.* , utiliser la commande suivante : 
`mvn archetype:generate -Dfilter=org.apache:`

- selectionner le template voulu et l'ajouter au fichier de conf lst-archetypes.txt

- le script generate-archetypes.sh sert à generer l'ensemble des templates maven (=archetypes) pouvant etre utile pour démarrer un projet dans les standards maven

### pour les dependences et les plugin
- modifier le pom.xml du projet-ref 
- executer le script execute-maven-cmd.sh . il permet de lancer l'ensemble des commandes maven sur le projet-ref afin de generer le repository. 

[TODO] utiliser docker pour lancer maven et generer le repository dans les différentes versions 
[TODO] trouver un moyen plus propre de recuperer plugin et dependances + javadocs et code sources.  

**-------------------------------------------------------------------------------------------------------------------------** 

**[old - idea ]** 

**-------------------------------------------------------------------------------------------------------------------------** 

pour utilisation dans différents environnement de maven : 

```
docker pull maven:3.3.9
docker pull maven:3.8.5
```


machine 1 : 
`docker run -it -v $HOME/.m2/repos:/root/.m2 maven:3.3.9 bash `

machine 2 : 
`docker run -it -v $HOME/.m2/repos:/root/.m2 maven:3.8.5 bash `

fichier de conf des archetypes : mvn-archetypes.txt
fichier de conf des commandes maven à checker : mvn-cmd.txt

ensuite il suffit de lancer : 



```
build-all-project.sh -[g|a|p PROJET]
-p : pour lancer les commandes maven du fichier mvn-cmd.txt sur un PROJET
-a : pour lancer les commandes maven du fichier mvn-cmd.txt sur les projets archetypes proj_*
-g : pour generer les archetypes du fichier mvn-archetypes.txt
```
